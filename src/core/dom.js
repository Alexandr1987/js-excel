class Dom {
  constructor(selector) {
    if (typeof selector === 'string') {
      this.$elem = document.createElement(selector);
    } else {
      this.$elem = selector;
    }
  }

  html(htmlContent) {
    // setter html to object
    if (typeof htmlContent === 'string') {
      this.$elem.innerHTML = htmlContent;
      return this;
    }
    return this.$elem.outerHTML.trim();
  }

  setAttr(atrr, value) {
    if (this.$elem!=undefined) {
      this.$elem.setAttribute(atrr, value);
    }
    return this;
  }

  clear() {
    this.$elem.html('');
    return this;
  }

  on(eventType, cb) {
    this.$elem.addEventListener(eventType, cb);
  }

  off(eventType, cb) {
    this.$elem.removeEventListener(eventType, cb);
  }


  append(node) {
    if (node instanceof Dom) {
      node = node.$elem;
    }
    if (Element.prototype.append) {
      this.$elem.append(node);
    } else {
      this.$elem.appendChild(node);
    }
    return this;
  }
}

export function $(element) {
  return new Dom(element);
}
$.createElement = (tag, classList = [])=>{
  const element = document.createElement(tag);
  if (classList) {
    element.classList.add(classList);
  }
  return $(element);
};
