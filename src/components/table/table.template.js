const {$} = require('../../core/dom');

const CODES = {
  CODE_A: 65,
  CODE_Z: 90,
};

export function table(columnCount = 10) {
  const table = [];
  for (let rowN = 0; rowN<columnCount; rowN++) {
    // make header
    if (rowN === 0) {
      table.push(makeHeaderRowTable(true));
    } else {
      table.push(makeNotHeaderRowTable(rowN));
    }
  }
  return table.join('');
}

const makeRows = (classRow = 'column', isHeader = false, rowNumber = '')=>{
  const headerRowsList = [];
  for (let i=CODES.CODE_A; i<CODES.CODE_Z; i++) {
    const headerColumn = makeClearDiv(classRow);
    if (isHeader) {
      headerColumn.html(String.fromCharCode(i));
    } else {
      headerColumn.setAttr('contenteditable', '');
      headerColumn.html(String.fromCharCode(i) + rowNumber);
    }

    headerRowsList.push(headerColumn.html());
  }
  return headerRowsList.join('');
};

const makeNotHeaderRowTable = (rowNumber)=>{
  const headerRowInfo = makeClearDiv('row-info');
  headerRowInfo.html(rowNumber.toString());
  const headerRowDataContainer = makeClearDiv('row-data');
  const headerContainer = makeClearDiv('row');
  headerContainer.html(
      headerRowInfo.html() +
      headerRowDataContainer.html(
          makeRows('cell', false, rowNumber)
      ).html()
  );
  return headerContainer.html();
};

const makeHeaderRowTable = () =>{
  const headerRowInfo = makeClearDiv('row-info');
  const headerRowDataContainer = makeClearDiv('row-data');
  const headerContainer = makeClearDiv('row');

  headerContainer.html(
      headerRowInfo.html() + headerRowDataContainer.html(
          makeRows('column', true)
      ).html()
  );
  return headerContainer.html();
};

const makeClearDiv = (className)=>{
  return $.createElement('div', className);
};
