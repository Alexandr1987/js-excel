import {$} from '@core/dom';

export class Excel {
  constructor(selector, options) {
    this.$el = document.querySelector(selector);
    this.components = options.components || [];
  }
  getRoot() {
    const $root = $.createElement('div', 'excel');
    this.components = this.components.map(Component=>{
      const $el = $.createElement('div', Component.className);
      const component = new Component($el);
      $el.html(component.toHtml());
      $root.append($el);
      return component;
    });
    return $root;
  }
  render() {
    this.$el.append(this.getRoot().$elem);
    this.components.forEach(component=>{
      component.init();
    });
  }
}
